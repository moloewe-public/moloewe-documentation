---
id: Selection_Page
title: Selection Page
custom_edit_url: null
---

# Selection Page
The user lands on this page when they have successfully logged in. On this page, the user can select between the Edit- and the Presentation-Mode.

<img src="./assets/front-end/SelectionPage.png" alt="selection-page"
	title="Select Edit or Presentation mode" width="auto" />

## Different Modes
## 1 Edit-Mode
In the edit mode, a presentation/a project can be created, worked on, changed & up- or downloaded. This mode is necessary, when planning and preparing a workshop. The following chapters with *Edit-* as a prefix explain the possible actions in detail.


## 2 Presentation-Mode
In this mode, a finished presentation can be presented or controlled. Each presentation needs one controller, otherwise it cannot be started. When a presentation is being carried out using MoLöWe, the audience members of the presentation interact with MoLöWe in Presentation Mode. 
When clicking on the **Presentation** button, there are four drop-down lists that need to be filled out.

<img src="./assets/front-end/PresentationPage.png" alt="presentation-page"
	title="Presentation page" width="auto" />

### Select project
This dropdown list contains all projects the user has access rights to and that are saved on the MoLöWe.

### Select screen 
This dropdown list shows all screens meaning all available 5 Monitors (for further clarification, see **Glossary**).

### Presentation
The user can choose between **Presentation** and **Controller** (see section 3) in this dropdown menu. To start a presentation, a controller is necessary. The controller has more rights and options during the presentation. There can be multiple presenters that cannot however control the presentation. 

### From Start
This dropdown list may not be clickable everytime, since it needs saved (previous) sessions as other options. If the presentation has not been presented yet or no earlier session was saved, then the presentation can only start from the start, so to say. If there are however old session that were saved (i.e. during a workshop at the end of a day) than the presentation can be started from that exact version.

To start the presentation, the user then clicks on the **Start** button after filling out all dropdown lists.

## 3 Controller Mode 
This mode can be entered on the presentation page in the third dropdown list. 
When a presentation is being carried out using MoLöWe, the presenter interacts with MoLöWe in Controller Mode.
Controller Mode allows users to change the Slides that are associated with Screens. Additionally, the Controller Mode also allows users to change the Screens that are placed on the different Display Devices.
