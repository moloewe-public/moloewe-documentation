---
id: tool_map
title: Tool - Map
custom_edit_url: null
---

A Map Tool is used to display a map created using Plotly during a presentation. Much like the [Graph Tool](tool_graph), it supports input from other tools such as Dropdown Tool and Slider Tool.

The Map tool requires definition of a function on the Project Notebook.

## Similarity to the Graph Tool

The maps created using the Map tool are essentially Plotly charts. As a result, many aspects of the way the map tool is defined and used bear resemblance to the Graph Tool. Hence, readers are advised to read the documentation on the Graph Tool to learn about the Map Tool using the following links:

- [**Defining Graph Tool on Project Notebook**](tool_graph#defining-graph-tool-on-project-notebook)
  - *Unlike the Graph Tool, the functions that relate to the Map Tool are placed under a class named `tool_maps`.*
- [**Loading Data from Files or URLs**](tool_graph#loading-data-from-files-or-urls)
- [**Viewing a Graph during Presentation**](tool_graph#viewing-a-graph-during-presentation)
- [**Combining a Graph with Other Tools**](tool_graph#combining-graphs-with-other-tools)
- [**Placing Graph on a Slide (Edit Mode)**](tool_graph#placing-graph-on-a-slide-edit-mode)
  - *On the icon for the map tool on the tool selection panel is shown in the image below. Besides that, all the steps/details are the same as the Graph Tool.*

<img src="./assets/front-end/64_map.jpg" alt="map-tool-selection-panel" title="Example Map Tool in Edit Mode" id="responsive-image-small" />

## Other Details Pertaining to the Map Tool

Below are some details that pertain specifically to the map tool.

- In the current implementation of MoLöWe, only Plotly maps are supported. Users are referred to the [*Official Documentation from Plotly*](https://plotly.com/python/maps/) for more information about creating maps using Plotly and Python.
- If [*Mapbox*](https://plotly.com/python/mapbox-layers/) is being used to create maps, an API Key (or Access Token) from Mapbox might be required.
- See [*Project Notebook*](project_notebook#214-definition-of-api-tokens-and-keys) for information on how and where to put the API Key.
- See [*Official Documentation from Mapbox*](https://docs.mapbox.com/help/getting-started/access-tokens/) to learn how to create an access token on Mapbox.
- Below is an example of the code that needs to be written in the Project Notebook to use the Map Tool:

```python
class tool_map:
    def get_lg_bank_library_map(self):
        
        # 1. Load global variables
        global data
        global mapbox_access_token  # defined on the top of the noteboos
        
        # 2. Read the data uploaded to MoLöWe
        # map_data.csv is uploaded from the Front-End (Files Page)
        df = pd.read_csv(data + "map_data.csv")

        csv_lat = list(df["lat"])
        csv_lon = list(df["lon"])
        csv_place = list(df["place"])
        
        # 3. Create plotly figure
        fig = go.Figure()

        fig.add_trace(
            go.Scattermapbox(
                lat=csv_lat,
                lon=csv_lon,
                mode="markers",
                marker=go.scattermapbox.Marker(size=12),
                text=csv_place,
            )
        )

        fig.update_layout(
            autosize=False,
            width=900,
            height=900,
            hovermode="closest",
            margin=go.layout.Margin(l=0, r=0, b=0, t=0, pad=0),
            mapbox=go.layout.Mapbox(
                accesstoken=mapbox_access_token, # access token is used here
                bearing=0,
                center=go.layout.mapbox.Center(lat=53.24353, lon=10.40610), # center of the map
                pitch=0,
                zoom=13, # zoom level of the map
            ),
        )

        return fig.to_dict()
```
