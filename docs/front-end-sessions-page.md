---
id: front_end_sessions_page
title: 'Sessions Page'
custom_edit_url: null
---

Once you select a project, you can view the Sessions page of the project by clicking on "Sessions" on the left panel of the user interface.

<img src="./assets/front-end/50_sessions_page.jpg" alt="sessions_page"
	title="Sessions page of a MoLöWe Project" id="responsive-image-small" />

The sessions page displays the sessions associated with a selected project.

Until a new session has been saved from the **Controller Mode** during a presentation, the Sessions Page will not list any sessions.

If new sessions have been created, the Sessions page lists the different saved sessions and allows users to edit Session details and delete sessions.

## 1 Creating Sessions

Sessions can only be created from the **Controller Mode** during a presentation.

To create a session from the controller mode, click on the bottom on the top right that says "Manage Presentation".

<img src="./assets/front-end/54_create_sessions_controller_1.jpg" alt="create-session-controller-option"
	title="Controller Mode Creation of Session Step 1" width="auto" />

Then, click on the option that says "Save Session".

<img src="./assets/front-end/54_create_sessions_controller_2.jpg" alt="create-session-controller-option"
	title="Controller Mode Creation of Session Step 2" width="auto" />

After that, an interface will appear where a name and description can be given to the session being saved. Here, Session Name is a required parameter.

<img src="./assets/front-end/55_create_sessions_controller_interface.jpg" alt="create-session-controller-interface"
	title="Controller Mode Interface for the Creation of Session" width="auto" />

After giving the session a name and/or a description, click on the Save button to confirm the save action.

If Sessions have been created, they will be listed on the Sessions page as shown below.

<img src="./assets/front-end/51_sessions_page_list.jpg" alt="sessions-list"
	title="Sessions of a MoLöWe Project" id="responsive-image-small" />

## 2 Editing Session Details

Sessions can be given custom names and descriptions. To edit these parameters of sessions, users can click on the "**i**" icon on the right hand side of the name of the session.

<img src="./assets/front-end/52_sessions_details_button.jpg" alt="sessions-edit-button"
	title="Edit Button for Sessions of a MoLöWe Project" id="responsive-image-small" />

Then, the user is taken to a page where the session details can be changed.

<img src="./assets/front-end/53_session_details_page.jpg" alt="change-slide-details-edit-page"
	title="Slide Details Editing Page" width="auto" />

From this page, the following details of the slides can be changed:

* Session Name
* Session Description

Once the changes have been made, the changes can be saved by pressing on the "Save" button on the right side of the page.

## 3 Accessing Saved Sessions

Saved sessions can only be accessed when starting a new presentation.

In order to restart a saved session, navigate to the presentation selection panel by pressing on a button on the bottom-left part of the MoLöWe front-end page.

<img src="./assets/front-end/10_start_presentation.jpg" alt="start-presentation"
	title="Starting a MoLöWe Presentation" width="auto" />

When this button is pressed, a new browser tab will open with a MoLöWe front-end page which allows users to start a presentation by selecting a project. Below is how that page looks.

<img src="./assets/front-end/11_presentation_selection_page.jpg" alt="presentation-selection-page"
	title="Selecting a MoLöWe Project for Presentation" id="responsive-image-small" />

From here, the drop-down menu on the bottom allows users to select sessions that they want to restore.  

By default, the "From Start" is selected, signifying that the presentation session will default to the settings that was defined by the creator of the project.  

<img src="./assets/front-end/56_restore_session_1.jpg" alt="saved-session-selection-option"
	title="Dropdown for Selecting Saved Session" id="responsive-image-small" />

However, when a user clicks on the fourth row, they will be presented with the available options.  

<img src="./assets/front-end/56_restore_session_2.jpg" alt="list-of-saved-sessions"
	title="List of Saved Sessions" id="responsive-image-small" />

Once a session has been selected, users can click on the "Start" button to restore presentation from the saved session.

<img src="./assets/front-end/56_restore_session_3.jpg" alt="presentation-selection-page"
	title="Starting a Presentation from a Saved Session" id="responsive-image-small" />

## 4 Deleting Sessions

The Session Details page described in [Step 2](front_end_sessions_page#2-editing-session-details) above also allows users to delete a given session by clicking on the "Delete" button on the left side of the page.
