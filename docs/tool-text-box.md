---
id: tool_text_box
title: Tool - Text Box
custom_edit_url: null
---

An Textbox Tool is used to display an text during a presentation.

The Textbox Tool does not require definition of a function on the Project Notebook.

## Uploading Text Files to MoLöWe

The text files that need to be placed on a slide can be uploaded from the Front-End with the [*process described here*](front_end_files_page#2-upload-a-new-file).

The supported file formats are `.txt`, `.md`, and `.html`.

## Placing a Text on a Slide (Edit Mode)

To place a Text Tool on a Slide, first upload the text file to MoLöWe. Then, open the slide in which the Textbox Tool needs to be placed in Edit Mode.

Then, open the tool selection panel in Edit Mode on the page that you want to place the tool in by following steps outlined [*here*](front_end_slides_page#4-editing-slides-edit-mode).

Following that, click on the Textbox icon.

<img src="./assets/front-end/69_textbox.jpg" alt="add-textbox-tool"
	title="Add Textbox Tool to a Slide in Edit Mode" width="auto" />

This will place a container for the Textbox Tool onto the slide.

<img src="./assets/front-end/38_tool_field.jpg" alt="Image-tool-field"
	title="Add Image Tool to a Slide in Edit Mode" width="auto" />

When a user hovers over this container, they will be presented with a gear icon that they can click on.

<img src="./assets/front-end/39_hover_tool_container.jpg" alt="tool-container-gear-icon"
	title="Tool Container Gear Icon" width="auto" />

Clicking on this icon opens an interface that allows users to select the textbox that they want to add to the slide.

<img src="./assets/front-end/69_textbox_1.jpg" alt="graph-tool-selection-interface"
	title="Interface to Select Textbox Tool" width="auto" />

The required fields in this interface are **Tool Name** and **Select File**. The *Tool Name* is a unique identifier for the tool, whereas the *Select File* presents a list of text files that the users can select from. The Header and Footer text are optional.

Once the tool name has been defined and an text file has been selected, users are taken back to the slide in Edit Mode.

Following this, the user can [resize](tools_overview#resize-a-molöwe-tool) or [reposition](tools_overview#reposition-a-molöwe-tool) the tool as they desire.

Do note that when the slide is in Edit Mode, a rectangular box appears around the tool. This box does not appear around the tool when the slide is viewed in Presentation or Controller Modes.

## Viewing an Textbox during Presentation

During presentation (i.e. while at Presentation Mode or Controller Mode), users can view the Textbox tool that was defined in the Edit Mode using the steps described above.

The HTML (`.html`) files and Markdown (`.md`) files are rendered based on the plain HTML syntax and can be used to present various types of text such as code snippets, headings, etc. in a visually interesting manner.

The plain text files (`.txt`) are not rendered in any special way. They look plain.
