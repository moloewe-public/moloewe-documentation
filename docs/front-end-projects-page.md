---
id: front_end_projects_page
title: 'Projects Page'
custom_edit_url: null
---

Once you have logged in to MoLöWe you will see a list of projects that you have access to (see below). This page is called Projects page and it allows users to select a project and start working on it, or create a new project, or import a MoLöWe project that was exported earlier.

<img src="./assets/front-end/02_projects_page.jpg" alt="projects-page"
	title="Projects Page of MoLöWe" id="responsive-image-small" />

In this section, all the functionality of the Projects page are discussed.

## 1 Creating a new Project

If a user has appropriate access rights, she can create a new project by pressing on the "Create Project" button below the list of projects.

<img src="./assets/front-end/12_create_project.jpg" alt="create-import-projects"
	title="Create or Import Projects" width="auto" />

## 2 Editing Details of a Project

To edit the details of a project (e.g. project name, project description, etc.), first navigate to the projects page by clicking on the Projects link on the top left of the screen.

<img src="./assets/front-end/15_go_to_projects_page.jpg" alt="visit-projects-page"
	title="Visit Projects Page" id="responsive-image-small" />

From the list of projects, identify the name of the project whose details need to be changed. In this example, we want to change the details of a project named "Font + Line Width Tests". 

Then click on an icon with an "i" on the right side of the project name (next to the pencil icon).

<img src="./assets/front-end/16_change_project_information.jpg" alt="change-project-details"
	title="Change Project Details" width="auto" />

Then, the user is taken to a page where the project details can be changed.

<img src="./assets/front-end/17_project_details_edit_page.jpg" alt="change-project-details-edit-page"
	title="Project Details Editing Page" id="auto" />

From this page, the following details of the project can be changed:

* Project Name
* Description
* Author Name
* Project Status

Once the changes have been made, you can save the changes by pressing on the "Save" button on the right side of the page.

This page also allows users to delete or export the project by clicking on respective buttons.

## 3 Selecting a Project

A project can be selected by clicking on the name of the project. 

Alternatively, one can also press on the pencil icon to the right of the project name to select a project. When a project is selected, the details of the project are shown to the user (see below). 

(In this example, a project named "Font + Line Width Test" was selected.)

<img src="./assets/front-end/04_project_details.jpg" alt="project-details"
	title="Details of a MoLöWe Project" id="responsive-image-small" />

When a project has been selected, the different sub-pages that belong to the project from which you can upload your custom notebook and files, and create slides can be accessed by clicking on their names on the left side of the page.

<img src="./assets/front-end/14_select_pages.jpg" alt="project-element-selection-panel"
	title="Project Element Selection Panel" id="responsive-image-small" />


## 4 Importing a Project

MoLöWe projects can be exported as a zip file. If a user already has a project that was exported from MoLöWe previously, they can import the entire projects using the "Import Project" button next to the "Create Project" button.

<img src="./assets/front-end/13_import_project.jpg" alt="create-import-projects"
	title="Create or Import Projects" width="auto" />

## 5 Deleting or Exporting a Project

Navigate to the page where project details can be changed by following steps detailed in [Step 3 above](front_end_projects_page#2-editing-details-of-a-project).

This will lead to a page where you can edit details of the project (see below).

<img src="./assets/front-end/17_project_details_edit_page.jpg" alt="change-project-details-edit-page"
	title="Project Details Editing Page" id="responsive-image-small" />

The same page also allows users to delete or export a given project. This can be done by pressing the related buttons on the bottom left side of the project details (see below).


<img src="./assets/front-end/18_delete_export_project.jpg" alt="delete-export-project"
	title="Delete or Export a Project" width="auto" />