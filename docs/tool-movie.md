---
id: tool_movie
title: Tool - Movie
custom_edit_url: null
---

An Movie Tool is used to display a video during a presentation.

The Movie Tool does not require definition of a function on the Project Notebook.

## Uploading Movie/Video Files to MoLöWe

The videos that need to be placed on a slide can be uploaded from the Front-End with the [*process described here*](front_end_files_page#2-upload-a-new-file).

## Placing Movie Tool on a Slide (Edit Mode)

To place a Movie Tool on a Slide, first upload the video to MoLöWe. Then, open the slide in which the Movie Tool needs to be placed in Edit Mode.

Then, open the tool selection panel in Edit Mode on the page that you want to place the tool in by following steps outlined [*here*](front_end_slides_page#4-editing-slides-edit-mode).

Following that, click on the Movie icon.

<img src="./assets/front-end/67_movie.jpg" alt="add-movie-tool"
	title="Add Movie Tool to a Slide in Edit Mode" width="auto" />

This will place a container for the Movie Tool onto the slide.

<img src="./assets/front-end/38_tool_field.jpg" alt="Movie-tool-field"
	title="Add Movie Tool to a Slide in Edit Mode" width="auto" />

When a user hovers over this container, they will be presented with a gear icon that they can click on.

<img src="./assets/front-end/39_hover_tool_container.jpg" alt="tool-container-gear-icon"
	title="Tool Container Gear Icon" width="auto" />

Clicking on this icon opens an interface that allows users to select the video file that they want to add to the slide.

<img src="./assets/front-end/67_movie_1.jpg" alt="movie-tool-selection-interface"
	title="Interface to Select Movie Tool" width="auto" />

The required fields in this interface are **Tool Name** and **Select Video**. The *Tool Name* is a unique identifier for the tool, whereas the *Select Video* is a list of video files that the users can select from. The Header and Footer text are optional.

Once the tool name has been defined and a video file has been selected, users are taken back to the slide in Edit Mode.

Following this, the user can [resize](tools_overview#resize-a-molöwe-tool) or [reposition](tools_overview#reposition-a-molöwe-tool) the tool as they desire.

Do note that when the slide is in Edit Mode, a rectangular box appears around the tool. This box does not appear around the tool when the slide is viewed in Presentation or Controller Modes.

## Viewing an Movie Tool during Presentation

During presentation (i.e. while at Presentation Mode or Controller Mode), users can view the Movie tool that was defined in the Edit Mode using the steps described above.
