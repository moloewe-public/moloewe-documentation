---
id: FAQ_german
title: FAQ_Deutsch
custom_edit_url: null
---

# FAQ

### Kann man die Mobile Lösungswerkstatt mieten?
Die Mobile Lösungswerkstatt ist momentan kein Service der Leuphana in diesem Sinne sondern eine Infrastruktur, die im Rahmen von Forschungsprojekten und Lehre eingesetzt wird. Workshops in der Mobilen Lösungswerkstatt sind also Teil von Forschungsprojekten oder Projektseminaren. Als Partner in einem Forschungsprojekt wird man von Anfang an in die Gestaltung der Workshops mit eingebunden.

### Was für eine Software nutzt die Mobile Lösungswerkstatt?
Für die Mobile Lösungswerkstatt wurde eigens eine Software für ein interaktives Browser basiertes Dashboard entwickelt. Backend- und Frontend-Entwicklung bauen auf einem erweiterbare Framework mit offenen Standards auf. Für interaktiven Graphen greift die Software beispielsweise auf plotly zurück, eine Plattform zur Online Datenanalyse, und für interaktive Karten auf mapbox, einem Anbieter von online Kartenmaterial. Zur Steuerung dieser interaktiven Datenvisualisierung wurde eigens für die Mobile Lösungswerkstatt Steuerungselemente, wie Schalter und DropDown-Schaltflächen, entwickelt. 

### Wie mobil ist die Mobile Lösungswerkstatt?
Die gesamte Technik lässt sich in rollbare Transportkisten verpacken und an beliebigem Ort wieder aufbauen. Voraussetzung ist ein barrierefreier Zugang (die Mobile Lösungswerkstatt kann keine Treppen überwinden), Internet- und Stromanschluss. Eine weitere Einschränkung ist der Einsatz allein in Gebäuden. Die Technik der Mobilen Lösungswerkstatt ist nicht für den Außeneinsatz konstruiert. Die Mobile Lösungswerkstatt ist modular und läßt sich demnach beliebig in der Zahl und Größe der Bildschirme variieren. Für den kompletten Aufbau mit fünf 65’’ Bildschirmen passt die Mobile Lösungswerkstatt in einen 3,5 t Transporter.

### Was ist die Zielgruppe der Mobilen Lösungswerkstatt?
Je nach Forschungsfrage sprechen die Workshops in der Mobilen Lösungswerkstatt ganz unterschiedliche Zielgruppen sein. Die Teilnehmer_innen eines Workshops können Mitarbeiter_innen der Stadtverwaltung oder eines Unternehmens, oder Wissenschaftler_innen, Poliker_innen aber auch Schüler_innen sein. Die Inhalte der Mobilen Lösungswerkstatt lassen sich an jedes Thema und an jede Zielgruppe anpassen.

### Wieviel Zeit muss ich als Teilnehmer_in für einen Workshop einplanen?
Ein Projekt in der Mobilen Lösungswerkstatt besteht aus mindestens zwei Workshops. Zum ersten Termin werden ersten Modelle und Lösungsoptionen von den Wissenschaflter_innen vorgestellt und gemeinsam mit den Teilnehmer_innen diskutiert und weiterentwickelt. Mit diesen Rückmeldungen bereiten die Wissenschaflter_innen dann den nächsten Workshop Termin vor, der die Strategien zur Umsetzung dieser Optionen zum Thema hat. Themenbedingt kann ein Workshop einen halben Tag oder ein bis zwei Tage in Anspruch nehmen.


<img src= "docs/assets/homepage/MoLöwe.png" alt="monitors"
	title="Monitors of MoLöWe" id="responsive-image-small" />

