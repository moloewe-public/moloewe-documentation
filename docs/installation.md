---
id: installation
title: Installation Procedure
custom_edit_url: null
---

The process for installing MoLöWe on a server have been outlined in the relevant GitLab repositories. Please contact the project lead for more information about accessing the instructions.
