---
id: tools_overview
title: Tools Overview
custom_edit_url: null
---

All the elements of MoLöWe that can be placed on a [Slide](front_end_slides_page) are called "MoLöWe Tools" or "Tools". These tools are visible during presentations and can be used to display information or to allow presentation visitors to interact with other MoLöWe Tools.

## Creating MoLöWe Tools

MoLöWe Tools can be created in one of two ways: (i) define the tools from the front end while at Edit Mode, (ii) define the tools on the Project Notebook and place them on the slides from the front end. As you one can see, in both the cases, the only way to place the tool on a slide (no matter where they were defined) is from the front-end while at [*Edit Mode*](front_end_slides_page#4-editing-slides-edit-mode).

### Tools without Project Notebook

Some MoLöWe Tools do not need any code on the Project Notebook in order for them to function properly. The tools that don't need a code from the Project Notebook are listed below.

* **Tool Button**: Many functions of buttons can be accessed and used without a code defined in the Project Notebook. *Note that some functions of buttons require MoLöWe code.* This tool has been described in detail [*here*](tool_button).
* **Tool Image**: If an image file has been uploaded to MoLöWe using the front-end Image tools can be placed on the slides as desired. This tool has been described in detail [*here*](tool_image).
* **Tool Movie**: If a video file has been uploaded to MoLöWe using the front-end Movie tools can be placed on the slides as desired. This tool has been described in detail [*here*](tool_movie).
* **Tool Slideshow**: If multiple image files have been uploaded to MoLöWe using hte font-end, Slideshow tools can be placed on the slides as desired. This tool has been described in detail [*here*](tool_slideshow).
* **Tool Textbox**: If a text file has been uploaded to MoLöWe using the front-end Textbox tools can be placed on the slides as desired. This tool has been described in detail [*here*](tool_text_box).

The tools above (except Tool Button) require files to be uploaded to MoLöWe using the Front End of the tool. Once the files have been uploaded, these tools can be defined in a [Slide](front_end_slides_page) by opening the slide in [*Edit Mode*](front_end_slides_page#4-editing-slides-edit-mode).

### Tools with Project Notebook

Some MoLöWe Tools need Python code written in a specific structure. The tools that require a code from the Project Notebook are listed below.

* **Switch Tool**: This tool has been described in detail [*here*](tool_switch).
* **Dropdown List Tool**: This tool has been described in detail [*here*](tool_drop_down).
* **Button Tool**: Some functions of the Button Tool require code from the Project Notebook. This tool has been described in detail [*here*](tool_button).
* **Range Slider Tool**: This tool has been described in detail [*here*](tool_range_slider).
* **Map Tool**: This tool has been described in detail [*here*](tool_map).
* **Graph Tool**: This tool has been described in detail [*here*](tool_graph).

The tools defined using the Project Notebook are defined as either a `get_` function or a pair of `get_` and `set_` functions. All the functions that belong to a category of tools are placed within a single dedicated Python `class`. Hence, a Python class is defined for each of the tool mentioned here. For instance, if one needs to create 2 different Plotly charts, they need to place two `get_` functions with unique names (eg. `get_first_chart` and `get_second_chart`) under a single class named `tool_graph`.

Once all the necessary tools have been defined in the Notebook and the Project Notebook has been [*uploaded*](front_end_notebooks_page) to MoLöWe, these tools can be created in a [Slide](front_end_slides_page) by opening the slide in [*Edit Mode*](front_end_slides_page#4-editing-slides-edit-mode). More details are presented on the pages that describe each tool.

## Resize a MoLöWe Tool

The once a tool has been placed on a slide, it can be resized by hovering the mouse pointer on the bottom right of the tool until the mouse pointer turns into an arrow with two points as shown below.

<img src="./assets/front-end/57_resize_tool.jpg" alt="files_page"
	title="Resize tools in MoLöWe" id="responsive-image-small" />

Then, click and drag the mouse pointer vertically or horizontally to change the size of the tool as desired.

## Reposition a MoLöWe Tool

The once a tool has been placed on a slide, it can be repositioned by clicking (with a mouse pointer) on the center of the tool and dragging the pointer on the slide. When the user starts dragging a tool, a rectangular grid structure will appear to aide the user in positioning the tool on the slide.

## Delete a MoLöWe Tool

To delete a MoLöWe tool that has been placed on a slide, (while at Edit Mode) hover over on the top right of the tool container until a gear icon appears.

<img src="./assets/front-end/39_hover_tool_container.jpg" alt="tool-container-gear-icon"
	title="Tool Container Gear Icon" width="auto" />

When this icon is clicked, an interface that contains the settings of the tool opens. A button on the bottom left of this interface lets users delete the tool from the slide.

In the example below, a textbox tool is being deleted from a slide.

<img src="./assets/front-end/70_delete_tool.jpg" alt="tool-container-gear-icon"
	title="Tool Container Gear Icon" width="auto" />
