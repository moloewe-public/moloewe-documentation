---
id: front_end_notebooks_page
title: 'Notebooks Page'
custom_edit_url: null
---

Once you select a project, you can visit the Notebooks page of the project by clicking on "Notebooks" on the left panel of the user interface.

This page displays the Jupyter Notebook associated with a project that has been selected.

<img src="./assets/front-end/05_notebooks_page.jpg" alt="notebooks_page"
	title="Notebooks page of a MoLöWe Project" id="responsive-image-small" />

By default, a new project has a notebook named *Template Notebook* automatically placed on this page. Users can download this notebook and edit it for their project if they so choose. The steps for downloading the *Template Notebook* are outlined in [Section 3 below](front_end_notebooks_page#3-download-a-project-notebook).

## 1 Upload a new Notebook

Users can upload a new notebook for their project by clicking on the "Import Notebook" button.

<img src="./assets/front-end/19_import_notebook.jpg" alt="import_notebook"
	title="Import a new notebook into a MoLöWe Project" width='auto' />

When the "Import Notebook" button is clicked, users are presented with an interface (see below) that allow uploading a new notebook.

<img src="./assets/front-end/20_import_notebook_interface.jpg" alt="import_notebook_interface"
	title="Interface to Import a new Project Notebook" id="responsive-image-small" />

From this interface, a new notebook can be uploaded from a user's computer by clicking on "Click here to select file ..." button as shown below.

<img src="./assets/front-end/21_select_notebook_upload.jpg" alt="select_notebook"
	title="Select Notebook to Import to a Project" width='auto' />

Once a new notebook has been selected, the changes can be saved by clicking on the save button on the bottom right side of the page.


## 2 Edit Project Notebook Details

Notebooks can be given custom descriptions. This can be done by clicking on the "**i**" icon on the right hand side of the name of the notebook.

<img src="./assets/front-end/22_change_notebook_details.jpg" alt="change_notebooks_details_button"
	title="Change Notebook Details" width='auto' />

When that button is pressed, the interface changes into a page that allows for the notebook description to be changed.

<img src="./assets/front-end/23_change_notebook_details_interface.jpg" alt="change_notebooks_details_interface"
	title="Interface to change Notebook Details" width='auto' />

Once a description text has been typed in to the description box, the changes can be saved by clicking on the "Save" button.

## 3 Download a Project Notebook

To download a project notebook, enter the interface that allows users to change notebook details by following [Section 2 outlined above](front_end_notebooks_page#2-edit-project-notebook-details). This should take users to the following page.

<img src="./assets/front-end/23_change_notebook_details_interface.jpg" alt="change_notebooks_details_interface"
	title="Interface to change Notebook Details" width='auto' />

To download a project notebook, click on the "Export Notebook" button.

## 4 Delete a Project Notebook

To delete a project notebook, enter the interface that allows users to change notebook details by following [Section 2 outlined above](front_end_notebooks_page#2-edit-project-notebook-details). This should take users to the following page.

<img src="./assets/front-end/23_change_notebook_details_interface.jpg" alt="change_notebooks_details_interface"
	title="Interface to change Notebook Details" width='auto' />

To delete a project notebook, click on the "Delete Notebook" button.