---
id: front_end
title: Front End Overview
custom_edit_url: null
---

The *front-end* of MoLöWe refers to all the pages that you see when you visit MoLöWe on your web browser. 

The front-end allows users to access, create, and edit projects. Further, provided that a user has Project Edit rights, the front-end also allows her to upload new MoLöWe notebooks, upload files, and setup slides and screens. Finally if a user has presentation access rights, the font-end also allows users to start or view interactive presentations from different projects.

Users are assigned the required access rights by the administrator of MoLöWe.

The pages that constitute the front-end are described in this section.

## 1 Login Page

Login page is where you log in to the MoLöWe system via your web browser.

<img src="./assets/front-end/01_login_page.png" alt="login-page"
	title="Login Page of MoLöWe" id="responsive-image-small" />

If you are using MoLöWe for the first time, this will be the first page that you encounter. This is also the page that you will see when you [**visit MoLöWe**](http://moloewe.leuphana.de) when you have not logged in yet.

:::caution

The user account and password protection feature in MoLöWe should not be considered a secured access and data stored there to be safe from unauthorized access and deletion. The user account system in MoLöWe is primarily in place to organize the access to different functions of it.

:::


## 2 Projects Page

This is the page that a user sees when they log in to MoLöWe via the front-end. In this page, all projects associated with the user are listed.

<img src="./assets/front-end/02_projects_page.jpg" alt="projects-page"
	title="Projects Page of MoLöWe" id="responsive-image-small" />

You can click on the name of the project that you want to access. When you do so, you are presented with the details of the project. 

(In this example, a project named "Font + Line Width Test") was selected.

<img src="./assets/front-end/04_project_details.jpg" alt="project-details"
	title="Details of a MoLöWe Project" id="responsive-image-small" />

When you have selected a project, you can access the different sub-pages that belong to the project from which you can upload your custom notebook and files, and create slides. The different pages related to the selected project are discussed below.

Further information on the different features of the Projects page is presented in its own section: [**Projects Page**](front_end_projects_page).

### 2.1 Notebooks Page

This page lists the Jupyter Notebook associated with the selected project.

Users can upload a new notebook for their project using the "Import Notebook" button.

<img src="./assets/front-end/05_notebooks_page.jpg" alt="notebooks_page"
	title="Notebooks page of a MoLöWe Project" id="responsive-image-small" />

Further information on the different features of the Projects page is presented in its own section: [**Notebooks Page**](front_end_notebooks_page).

### 2.2 Files Page

This page lists all the files that have been uploaded to a project.

Users can upload a new file by using the "Import File" button.

<img src="./assets/front-end/06_files_page.jpg" alt="files_page"
	title="Files page of a MoLöWe Project" id="responsive-image-small" />

Further information on the different features of the Projects page is presented in its own section: [**Files Page**](front_end_files_page).

### 2.3 Slides Page

This page lists all the slides that have been created in a project. When a new project is created, an empty slide with the name "Slide 1" will automatically be created.

New slides can be created by pressing the "Create Slide" button.

<img src="./assets/front-end/07_slides_page.jpg" alt="slides_page"
	title="Slides page of a MoLöWe Project" id="responsive-image-small" />

Users can edit slide contents to fit their presentation requirements by entering the Edit Mode by selecting slides from this page. Further information on this and the other features of the Slides page is presented in its own section: [**Slides and Screens Page**](front_end_slides_screens_page).

### 2.4 Screens Page

This page lists all the screens that have been created in a project. 

When a new project is created, an empty slide with the name "Screen 1" will automatically be created. By default, Screen 1 has Slide 1 as its start slide.

<img src="./assets/front-end/08_screens_page.jpg" alt="screens_page"
	title="Screens page of a MoLöWe Project" id="responsive-image-small" />

Further information on the different features of the Screens page is presented in its own section: [**Slides and Screens Page**](front_end_slides_screens_page).

### 2.5 Sessions Page

This page lists all the sessions that have been created during a presentation session of a project.

<img src="./assets/front-end/09_sessions_page.jpg" alt="sessions_page"
	title="Sessions page of a MoLöWe Project" id="responsive-image-small" />

Further information on the different features of the Sessions page is presented in its own section: [**Sessions Page**](front_end_sessions_page).

## 3 Starting a Presentation

Users can start a presentation by pressing on a button on the bottom-left part of the MoLöWe front-end page.

<img src="./assets/front-end/10_start_presentation.jpg" alt="start-presentation"
	title="Starting a MoLöWe Presentation" width="auto" />

When this button is pressed, a new browser tab will open with a MoLöWe front-end page which allows users to start a presentation by selecting a project. Below is how that page looks.

<img src="./assets/front-end/11_presentation_selection_page.jpg" alt="presentation-selection-page"
	title="Selecting a MoLöWe Project for Presentation" id="responsive-image-small" />

## 4 Logging Out of MoLöWe

Users can log out from MoLöWe at any time by clicking on the icon on the top right of the pages in the front-end next to the user name.

<img src="./assets/front-end/9999_log_out.jpg" alt="projects-page"
	title="Projects Page of MoLöWe" width="auto" />
