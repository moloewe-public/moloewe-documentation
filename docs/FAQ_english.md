---
id: FAQ_english
title: FAQ
custom_edit_url: null
---

# FAQ

### Is the Mobile Solution Workshop for rent?
Currently the Mobile Solution Workshop is not a service provided from Leuphana in this sense, instead it is an infrastructure that is used in the context of research projects and teaching. Therefore, events in the Mobile Solution Workshop are part of research projects or project seminars. As a partner in a research project, your are involved in the design of the workshop from the start.

### What software does the Mobile Solution Workshop use?
The software for this interactive browser based dashboard was custom developed for the Mobile Solution Workshop. Backend and fronted development are based on expandable frameworks with open standards. For example, for interactive graphs the software uses plotly, a provider of online data analytics and visualisation tools, and for interactive maps mapbox, a provider for custom online maps. To control these interactive data visualisation control elements, such as switches and dropdown lists, were developed for the Mobile Solution Workshop.

### How mobile is the Mobile Solution Workshop?
The entire hardware set-up can be packed into rollable boxes and set up again on location. The prerequisites are a barrier-free access (the Mobile Solution Workshop cannot climb stairs), internet and power access. It is only designed for indoors. The Mobile Solution Workshop is modular and can therefore be scaled as needed in the number and size of screens. For the complete set-up including five 65’’ screens the Mobile Solution Workshop fits into a 3.5t van. 

### What is the target group of the Mobile Solution Workshop?
Depending on the research question projects in the Mobile Solution Workshops addresses all kinds of target groups. Workshop participants can be employees of the city administration or a private company, or scientists, policymakers, but also pupils. Contents of the Mobile Solution Workshop can be adapted to any topic or target group.

### How much time do I have to invest as a workshop participant?
A project in the Mobile Solution Workshop consist of at least two events. For the first event, preliminary models and options for solutions will be presented by the researchers and discussed and further developed with participants. Based on this feedback, researchers then prepare the next event, which focuses on the implementation strategies for discussed options. Depending on the topic one event can take half a day or 1 to 2 days. 

<img src= "docs/assets/homepage/MoLöwe.png" alt="makers_space"
	title="Monitors of MoLöWe" id="responsive-image-small" />
    
