---
id: intro_german
title: Mobile Lösungswerkstatt
custom_edit_url: null
---

# Mobile Lösungswerkstatt 

## Eine interaktive, digitale Plattform, für partizipative Lösungsfindung.

---

### Kurze Zusammenfassung 

Die Mobile Lösungswerkstatt wurde als neues Workshop Format an der Fakultät Nachhaltigkeit entwickelt.
Diese digital unterstützte Umgebung erlaubt beispielsweise Zukunftsszenarien zu visualisieren und die Auswirkung von Modellveränderung in Echtzeit erlebbar zu machen. Sie besteht aus der Technik, mit fünf 65’’ Bildschirmen, die es ermöglichen in Situationen einzutauchen und Tablets, mit denen Teilnehmer_innen mit Inhalten interagieren können, sowie einer eigens für das interaktive Dashboard entwickelten Software. Die Technik ist mobil und somit bei den Akteuren vor Ort einsetzbar. Schon deshalb können Wissenschaftler_innen mit der Mobilen Lösungswerkstatt größere Zielgruppen erreichen. 
Über fünf verbundene 65’’ Bildschirme werden komplexe Probleme in verständliche Schritte aufgefächert und visualisiert. Teilnehmer_innen können dadurch interaktiv simulierte Szenarien erfahren und selbst ausgestalten. Die Mobile Lösungswerkstatt erlaubt somit in Echtzeit die Auswirkungen von Maßnahmen zu visualisieren (Überwinden zeitlicher Skalen); das Wechselspiel zwischen lokalem Handeln und globalen Entwicklungen zu verdeutlichen (Überwinden räumlicher Skalen) sowie abstrakte Sachverhalte greifbar und erlebbar zu machen (Überwindung der Lücke zwischen abstraktem Wissen und konkretem Planen, Entscheiden und Handeln).

---
### Die Mobile Lösungswerkstatt kombiniert vier Stufen der Beteiligung:

* Information = über fünf verbundene 65’’ Bildschirme werden komplexe Probleme in verständliche Schritte von den Wissenschaftler_innen aufgefächert und visualisiert.

* Konsultation = Wissenschaftler_innen können in der Mobilen Lösungswerkstatt mit den Workshop Teilnehmer*innen ihre Modelle dem Praxistest unterziehen oder Rückmeldung für geplante Maßnahmen sammeln.

* Kollaboration = Teilnehmer_innen können interaktiv simulierte Szenarien erfahren selbst ausgestalten und gemeinsam mit den Wissenschaftler_innen Lösungsoptionen entwickeln.

* Empowerment = Implementierungsstrategien werden partizipativ entwickelt und diese wiederum visualisiert. 

<img src="./assets/homepage/MLW-Leveragepoints-1.JPG" alt="monitors"
	title="Monitors of MoLöWe" id="responsive-image-small" />


