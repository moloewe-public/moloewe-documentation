---
id: intro_english
title: Mobile Solution Workshop
custom_edit_url: null
---


# Mobile Solution Workshop

## An interactive, digital platform for participative problem-solving

### Short summary
The Mobile Solution Workshop is a new approach for participative workshops developed at the Faculty of Sustainability. This digital environment allows to visualise future scenarios and to experience the effects of adjusted model parameters in real time. It consists of the hardware, including five 65’’ screens that create an immersive environment and tablets that allow participants to interact with the screens’ content, as well as the custom developed software for this interactive dashboard. The entire set-up is mobile and can therefore be put into action on site. For this reason alone, scientist can reach a larger target audience with the Mobile Solution Workshop. Complex problems are fanned out and visualised in comprehensible steps across the five connected 65’’ screens. Participants can experience and design interactive scenarios. To this end, the Mobile Solution Workshop allows to visualise effects of interventions in real-time (overcoming temporal boundaries); illustrates the interplay between local actions and global drivers (overcoming spatial boundaries) and to make abstract facts tangible (overcoming the gap between abstract knowledge and concrete planning, decision making and action). 

#### The Mobile Solution Workshop encompasses all four intensities of involvement:


* Information = scientists break down and visualise complex problems across five 65’’ screens into comprehensible steps.

* Consultation = in the Mobile Solution Workshop scientists can put their models to the practical test and receive participants’ feedback on interventions.

* Collaboration = participants can experience and design interactive scenarios to jointly develop solution with scientists. 

* Empowerment = implementation strategies are developed jointly and visualised.


<img src="./assets/homepage/MLW-Leveragepoints-1.JPG" alt="monitors"
	title="Monitors of MoLöWe" id="responsive-image-small" />

