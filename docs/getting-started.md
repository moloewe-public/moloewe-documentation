---
title: Getting Started with MoLöWe
slug: /
custom_edit_url: null
---

MoLöWe is an interactive visualization platform that is powered by open-source software and libraries like [Python 3](https://www.python.org/), [Jupyter Notebook](https://jupyter.org/),[Echarts](https://echarts.apache.org/en/index.html) and [Plotly](https://plotly.com/).



## Quick start video

Before you can start using MoLöWe, you need to ensure that you have the right softwares installed on your device.

## Quick start

:::info

If you are already familiar with MoLöWe, check out the documentation on the MoLöWe Notebook or on the specific tools

:::

- The Mobile Solution Workshop Software runs on the moloewe.leuphana.de server (remote access), as well as on the local machine at Leuphana.
- Ask for an user account (see contact details) and specify which system you want to use (they are two separate systems).
- Set up your own jupyter notebook environment to work on your notebook (see juypterhub).
