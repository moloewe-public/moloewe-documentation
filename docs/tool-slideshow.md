---
id: tool_slideshow
title: Tool - Slideshow
custom_edit_url: null
---

An Slideshow Tool is used to display multiple images in sequence (at a pre-defined interval) during a presentation.

The Slideshow Tool does not require definition of a function on the Project Notebook.

## Uploading Images to MoLöWe

The images that need to be placed on a slide using the Slideshow Tool can be uploaded from the Front-End with the [*process described here*](front_end_files_page#2-upload-a-new-file).

## Placing Slideshow Tool on a Slide (Edit Mode)

To place a Slideshow Tool on a Slide, first upload the images to MoLöWe. Then, open the slide in which the Slideshow Tool needs to be placed in Edit Mode.

Then, open the tool selection panel in Edit Mode on the page that you want to place the tool in by following steps outlined [*here*](front_end_slides_page#4-editing-slides-edit-mode).

Following that, click on the Slideshow icon.

<img src="./assets/front-end/68_slideshow.jpg" alt="add-slideshow-tool"
	title="Add Slideshow Tool to a Slide in Edit Mode" width="auto" />

This will place a container for the Image Tool onto the slide.

<img src="./assets/front-end/38_tool_field.jpg" alt="Image-tool-field"
	title="Add Image Tool to a Slide in Edit Mode" width="auto" />

When a user hovers over this container, they will be presented with a gear icon that they can click on.

<img src="./assets/front-end/39_hover_tool_container.jpg" alt="tool-container-gear-icon"
	title="Tool Container Gear Icon" width="auto" />

Clicking on this icon opens an interface that allows users to select the Image that they want to add to the slide.

<img src="./assets/front-end/68_slideshow_1.jpg" alt="graph-tool-selection-interface"
	title="Interface to Select Graph Tool" width="auto" />

The required fields in this interface are **Tool Name**, **Show Controls**, **Duration**, **Images**. The *Tool Name* is a unique identifier for the tool, *Show Controls* can be used to show or hide the slideshow controls during a presentation, *Duration* determines the frequency (seconds) at which the images change, and *Images* lets users select multiple images that need to be put under the slideshow. The Header and Footer text are optional.

Once the tool name has been defined and an images has been selected, users are taken back to the slide in Edit Mode.

Following this, the user can [resize](tools_overview#resize-a-molöwe-tool) or [reposition](tools_overview#reposition-a-molöwe-tool) the tool as they desire.

Do note that when the slide is in Edit Mode, a rectangular box appears around the tool. This box does not appear around the tool when the slide is viewed in Presentation or Controller Modes.

## Viewing an Slideshow during Presentation

During presentation (i.e. while at Presentation Mode or Controller Mode), users can view the different images that has been selected in the Slideshow tool that was defined.
