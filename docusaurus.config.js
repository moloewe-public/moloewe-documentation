/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'MobileLösungsWerkstatt (MoLöWe) / MobileSolutionWorkshop (MSW)',
  tagline: 'Platform Documentation',
  url: 'https://moloewe.leuphana.de/doc',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'MoLöWe', // Usually your GitHub org/user name.
  projectName: 'Documentation', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'MoLöWe',
      logo: {
        alt: 'MoLöWe Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Updates', position: 'left'},
        {
          href: 'http://moloewe.leuphana.de',
          label: 'Visit MoLöWe',
          position: 'right',
        },
      ],
    },
    colorMode: {
      defaultMode: 'light',
      disableSwitch: true,
      respectPrefersColorScheme: false,
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Documentation',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
            {
              label: 'Template Notebook',
              to: 'docs/',
            },
            {
              label: 'Front End',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://moloewe.leuphana.de/doc',
            },
            {
              label: 'Discord',
              href: 'https://moloewe.leuphana.de/doc',
            },
            {
              label: 'Twitter',
              href: 'https://moloewe.leuphana.de/doc',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Updates',
              to: 'blog',
            },
            {
              label: 'GitLab',
              href: 'https://moloewe.leuphana.de/doc',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} MobileLösungsWerkstatt (MoLöWe). Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
