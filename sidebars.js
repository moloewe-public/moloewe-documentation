module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Introduction',
      collapsed:false,
      items: [
        'getting-started',
        'setup-programming-environment',
        'project_notebook'
      ],
    },
    {
      type: 'category',
      label: 'MoLöWe Front-End',
      collapsed:false,
      items: [
        'front_end',
        'front_end_projects_page',
        'front_end_notebooks_page',
        'front_end_files_page',
        'front_end_slides_page',
        'front_end_screens_page',
        'front_end_sessions_page',
      ],
    },
    {
      type: 'category',
      label: 'MoLöWe Tools',
      collapsed:false,
      items: [
        'tools_overview',
        'tool_button', 
        'tool_drop_down', 
        'tool_graph', 
        'tool_image', 
        'tool_map', 
        'tool_movie', 
        'tool_range_slider', 
        'tool_slideshow',
        'tool_switch',
        'tool_text_box'
      ],
    },
    {
      type:"ref",
      id:"glossary"
    },
    {
      type: 'category',
      label: 'Developer Resources',
      collapsed:true,
      items: [
        'installation',
        'known-issues'
      ],
    },
  ],
};
